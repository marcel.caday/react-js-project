# REACT JS PROJECT

## Description

This project includes two different applications:

- Todo App
- Album App

Initially this was supposed to be a simple Todo App and the challenge for me was to write it in reactjs and typescript, and use other hooks such as routers and mob-x keystone in order to make up with the lacking features. However, I decided to add another application that would demonstrate API fetching which is the Album App.

## Features

### Todo application

- Add Todo task
- Mark the task done
- Edit Task
- Delete task

### Album application

- Home Page
- Album that shows all images of all photographers
- If image is clicked, will redirect to image details
- A page that list down all photographers and search bar on side to filter
- If name of the photographer is clicked, will redirect to a page that shows their works

## Instructions

- Enter the directory of the application
- yarn install
- yarn start

## Packages / Hooks

- reactjs-typescript project
- mob-x-keystone
- routers
