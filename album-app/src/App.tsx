import "mobx-react-lite/batchingForReactDom";
import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AlbumAuthorList from "./components/AlbumAuthorList";
import AuthorAlbum from "./components/AlbumAuthorDetail";
import AlbumAllAuthor from "./components/AlbumAllAuthor";
import AlbumMain from "./components/AlbumMain";
import ImageDetail from "./components/AlbumImageDetail";
import { createRootStore, StoreProvider } from "./models/index";

const App = () => {
  const album = createRootStore();

  useEffect(() => {
    album.fetchImages();
  }, [album]);

  return (
    <StoreProvider value={album}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={AlbumMain} />

          <Route exact path="/author/all" component={AlbumAllAuthor} />
          <Route exact path="/author/all/:id" component={ImageDetail} />

          <Route exact path="/author/list" component={AlbumAuthorList} />
          <Route exact path="/author/:imageAuthor" component={AuthorAlbum} />

          <Route render={() => <h1>404: page not found</h1>} />
        </Switch>
      </BrowserRouter>
    </StoreProvider>
  );
};

export default observer(App);
