import { model, Model, prop } from "mobx-keystone";

@model("Image")
export class Image extends Model({
  id: prop<string>(),
  author: prop<string>(),
  width: prop<number>(),
  height: prop<number>(),
}) {}
