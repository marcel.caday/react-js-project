import "mobx-react-lite/batchingForReactDom";
import React from "react";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";
import "../css/Home.css";
import AlbumNav from "./AlbumNav";
import logo from "../image/logo.png";
import homeHeader from "../image/homeHeader.png";

const AlbumMain = () => {
  return (
    <div className="home">
      <AlbumNav />
      <div className="header">
        <img src={homeHeader} className="logoHeader" alt="logo" />
        <h1 className="heading">Welcome to Web Shots!</h1>
        <p className="subHeading">
          A website dedicated to showcase
          <br />
          various photographer's art
          <br />
          that lets the audience see
          <br />
          the world through
          <br />
          their lenses
        </p>
      </div>

      <div className="wrapper">
        <h2 className="featureHeader"> Start browsing by</h2>
        <Link to="/author/all">
          <button className="buttonRedirect">Images</button>
        </Link>
        <Link to="/author/list">
          <button className="buttonRedirect">Author</button>
        </Link>
      </div>
    </div>
  );
};

export default observer(AlbumMain);
