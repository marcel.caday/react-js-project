/* eslint-disable @typescript-eslint/no-unused-expressions */
import React, { useContext, useState, useEffect } from "react";
import { observer } from "mobx-react-lite";

import { Item } from "../models/Item";
import Store from "../models/List";
import ToDoDelete from "./ToDoDelete";
import ToDoEdit from "./ToDoEdit";

import "../css/TodoItem.css";

interface TodoItemProp {
  item: Item;
  isChange: boolean;
  setIsChange: Function;
}

interface ItemProp {
  item: Item;
  isChange: boolean;
  setIsEdit: Function;
  setIsDelete: Function;
  setIsDone: Function;
  isDone: boolean;
}

interface TodoItemButtomProps {
  setIsEdit: Function;
  setIsDelete: Function;
}

const ItemButtons: React.FC<TodoItemButtomProps> = ({
  setIsEdit,
  setIsDelete,
}) => {
  return (
    <div>
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
      />
      <button className="editBtn" onClick={() => setIsEdit(true)}>
        <i className="fa fa-edit"></i>
      </button>
      <button className="deleteBtn" onClick={() => setIsDelete(true)}>
        <i className="fa fa-trash"></i>
      </button>
    </div>
  );
};

const ItemTodo: React.FC<ItemProp> = ({
  item,
  isChange,
  setIsEdit,
  setIsDelete,
  setIsDone,
  isDone,
}) => {
  const store = useContext(Store);
  const handleMarkDone = () => {
    console.log("check mark");
    store.markDoneItem(item.id);
    setIsDone(!isDone);
  };
  return (
    <div className="taskContainer">
      <div className="taskLabel">
        <form onSubmit={handleMarkDone}>
          <p
            onClick={handleMarkDone}
            style={
              item.isDone
                ? { textDecoration: "line-through" }
                : { textDecoration: "none" }
            }
          >
            {item.task}
          </p>
        </form>
      </div>
      <ItemButtons setIsEdit={setIsEdit} setIsDelete={setIsDelete} />
    </div>
  );
};

const TodoItem: React.FC<TodoItemProp> = ({ item, isChange, setIsChange }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [isDelete, setIsDelete] = useState(false);
  const [isDone, setIsDone] = useState(false);

  useEffect(
    () => setIsChange(isEdit || isDelete || isDone),
    [setIsChange, isEdit, isDelete, isDone]
  );

  return (
    <div className="todoItem">
      {isDelete ? (
        <ToDoDelete item={item} setIsDelete={setIsDelete} />
      ) : isEdit ? (
        <ToDoEdit item={item} setIsEdit={setIsEdit} />
      ) : (
        <ItemTodo
          item={item}
          isChange={isChange}
          setIsEdit={setIsEdit}
          setIsDelete={setIsDelete}
          setIsDone={setIsDone}
          isDone={isDone}
        />
      )}
    </div>
  );
};

export default observer(TodoItem);
