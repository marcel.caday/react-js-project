import React, { useContext, useState } from "react";
import Store from "../models/List";
import { observer } from "mobx-react-lite";
import TodoItem from "./TodoItem";

interface ListProps {
  taskStatus: boolean;
}

const TodoList: React.FC<ListProps> = ({ taskStatus }) => {
  const store = useContext(Store);
  const [isChange, setIsChange] = useState(false);

  return (
    <div>
      {store.itemList
        .filter((item) => item.isDone === taskStatus)
        .map((item, index) => (
          <TodoItem
            item={item}
            key={item.id}
            isChange={isChange}
            setIsChange={setIsChange}
          />
        ))}
    </div>
  );
};

export default observer(TodoList);
