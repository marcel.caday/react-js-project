import React, { useState, useContext, FormEvent, ChangeEvent } from "react";
import Store from "../models/List";
import { Item } from "../models/Item";
import { observer } from "mobx-react-lite";
import { v4 as uuidv4 } from "uuid";
import "../css/TodoAdd.css";

const TodoAdd = () => {
  const [task, setTask] = useState("");
  const store = useContext(Store);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setTask(e.target.value);
  };

  const addTodoTask = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault(); //stop page from refreshing
    store.addItem(new Item({ task: task, id: uuidv4() }));
    setTask("");
  };

  return (
    <div>
      <form onSubmit={addTodoTask}>
        <input
          className="addTaskInput"
          type="text"
          placeholder="Add Task"
          value={task}
          onChange={handleChange}
        />
        <button className="addTaskButton" disabled={task.length === 0}>
          +
        </button>
      </form>
    </div>
  );
};

export default observer(TodoAdd);
