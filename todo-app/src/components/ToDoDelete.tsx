import React, { useState, useContext, FormEvent, ChangeEvent } from "react";
import Store from "../models/List";
import { Item } from "../models/Item";
import { observer } from "mobx-react-lite";

import "../css/TodoItem.css";

interface TodoDeleteProp {
  item: Item;
  setIsDelete: Function;
}

const TodoDelete: React.FC<TodoDeleteProp> = ({ item, setIsDelete }) => {
  const store = useContext(Store);

  return (
    <div className="taskContainer">
      <div className="taskLabel">
        Delete
        <em>
          <b>{item.task}</b>
        </em>
        ?
      </div>
      <div>
        <button
          className="editBtn"
          onClick={() => {
            store.deleteItem(item.id);
            setIsDelete(false);
          }}
        >
          ✓
        </button>
        <button className="deleteBtn" onClick={() => setIsDelete(false)}>
          ✖
        </button>
      </div>
    </div>
  );
};
export default observer(TodoDelete);
