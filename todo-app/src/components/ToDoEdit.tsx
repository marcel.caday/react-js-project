import React, { useState, useContext, FormEvent, ChangeEvent } from "react";
import Store from "../models/List";
import { Item } from "../models/Item";
import { observer } from "mobx-react-lite";

import "../css/TodoItem.css";

interface TodoEditProp {
  item: Item;
  setIsEdit: Function;
}

const TodoEdit: React.FC<TodoEditProp> = ({ item, setIsEdit }) => {
  const store = useContext(Store);
  const [editList, setEditList] = useState(item.task);

  const handleTodoEdit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    store.editItem(item.id, editList);
    setIsEdit(false);
    setEditList("");
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setEditList(e.target.value);
  };

  return (
    <div className="taskContainer">
      <div className="taskLabel">
        <form onSubmit={handleTodoEdit}>
          <input
            type="text"
            className="editTaskInput"
            value={editList}
            onChange={handleChange}
          />
        </form>
      </div>
      <div className="taskContainer">
        <button className="editBtn" disabled={editList.length === 0}>
          ✓
        </button>
        <button className="deleteBtn" onClick={() => setIsEdit(false)}>
          ✖
        </button>
      </div>
    </div>
  );
};

export default observer(TodoEdit);
