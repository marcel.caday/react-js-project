import "mobx-react-lite/batchingForReactDom";
import React from "react";
import TodoList from "./TodoList";
import TodoAdd from "./TodoAdd";
import { observer } from "mobx-react-lite";
import "../css/TodoMain.css";

const TodoMain = () => {
  return (
    <div>
      <div className="wrapper">
        <h1 className="appHeader"> Todo App </h1>
        <TodoAdd />
      </div>
      <div className="todoList">
        <div className="todoListPending">
          <h3>PENDING TASKS</h3>
          <TodoList taskStatus={false} />
        </div>
        <div className="todoListCompleted">
          <h3>COMPLETED TASKS</h3>
          <TodoList taskStatus={true} />
        </div>
      </div>
    </div>
  );
};

export default observer(TodoMain);
