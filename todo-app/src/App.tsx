import "mobx-react-lite/batchingForReactDom";
import React, { useState } from "react";

import TodoMain from "./components/TodoMain";
import Store, { List } from "./models/List";

import { observer } from "mobx-react-lite";
import { BrowserRouter, Route, Switch } from "react-router-dom";

const App = () => {
  const [store] = useState(new List({}));

  return (
    <div>
      <Store.Provider value={store}>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={TodoMain} />
          </Switch>
        </BrowserRouter>
      </Store.Provider>
    </div>
  );
};

export default observer(App);
